
import numpy as np
import cv2
import matplotlib.pyplot as plt
from pynput.mouse import Controller
from pynput import keyboard
from scipy.ndimage.filters import gaussian_filter

key_dict = {
    keyboard.Key.esc: (0,0),
    keyboard.Key.media_volume_mute: (0, 1),
    keyboard.Key.f1: (0, 1),
    keyboard.Key.media_volume_down: (0, 2),
    keyboard.Key.f2: (0, 2),
    keyboard.Key.media_volume_up: (0, 3),
    keyboard.Key.f3: (0, 3),
    keyboard.Key.f4: (0, 4),
    keyboard.Key.f5: (0, 5),
    keyboard.Key.f6: (0, 6),
    keyboard.Key.f7: (0, 7),
    keyboard.Key.f8: (0, 8),
    keyboard.Key.f9: (0, 9),
    keyboard.Key.f10: (0, 10),
    keyboard.Key.f12: (0, 12),
    keyboard.Key.f11: (0, 11),
    keyboard.Key.pause: (0, 13),
    keyboard.Key.print_screen: (0, 14),
    keyboard.Key.insert: (0, 14),
    keyboard.Key.delete: (0, 15),
    keyboard.Key.tab: (2, 0),
    keyboard.Key.caps_lock: (3, 0),
    keyboard.Key.shift: (4, 0),
    keyboard.Key.ctrl: (5, 0),
    keyboard.Key.cmd: (5, 3),
    keyboard.Key.alt: (5, 4),
    keyboard.Key.space: (5,7),
    keyboard.Key.menu: (5, 11),
    keyboard.Key.ctrl_r: (5, 12),
    keyboard.Key.left: (5, 13),
    keyboard.Key.home: (5, 13),
    keyboard.Key.down: (5, 14),
    keyboard.Key.page_down: (5, 14),
    keyboard.Key.right: (5, 15),
    keyboard.Key.end: (5, 14),
    '\\': (1,0),
    '1': (1,1),
    '2': (1,2),
    '3': (1,3),
    '4': (1,4),
    '5': (1,5),
    '6': (1,6),
    '7': (1,7),
    '8': (1,8),
    '9': (1,9),
    '0': (1,10),
    '\'': (1,11),
    '«': (1,12),
    '|': (1,0),
    '!': (1,1),
    '"': (1,2),
    '#': (1,3),
    '$': (1,4),
    '%': (1,5),
    '&': (1,6),
    '/': (1,7),
    '(': (1,8),
    ')': (1,9),
    '=': (1,10),
    '?': (1,11),
    '»': (1,12),
    keyboard.Key.backspace: (1, 14),
    'q': (2,2),
    'w': (2,3),
    'e': (2,4),
    'r': (2,5),
    't': (2,6),
    'y': (2,7),
    'u': (2,8),
    'i': (2,9),
    'o': (2,10),
    'p': (2,11),
    'Q': (2,2),
    'W': (2,3),
    'E': (2,4),
    'R': (2,5),
    'T': (2,6),
    'Y': (2,7),
    'U': (2,8),
    'I': (2,9),
    'O': (2,10),
    'P': (2,11),
    '+': (2,12),
    '*': (2,12),
    '´': (2,13),
    '`': (2,13),
    keyboard.Key.enter: (2, 14),
    'a': (3,2),
    's': (3,3),
    'd': (3,4),
    'f': (3,5),
    'g': (3,6),
    'h': (3,7),
    'j': (3,8),
    'k': (3,9),
    'l': (3,10),
    'ç': (3,11),
    'A': (3,2),
    'S': (3,3),
    'D': (3,4),
    'F': (3,5),
    'G': (3,6),
    'H': (3,7),
    'J': (3,8),
    'K': (3,9),
    'L': (3,10),
    'Ç': (3,11),
    'º': (3,12),
    'ª': (3,12),
    '~': (3,13),
    '^': (3,13),
    '<': (4,2),
    '>': (4,2),
    'z': (4,3),
    'x': (4,4),
    'c': (4,5),
    'v': (4,6),
    'b': (4,7),
    'Z': (4,3),
    'n': (4,8),
    'm': (4,9),
    'X': (4,4),
    'C': (4,5),
    'V': (4,6),
    'B': (4,7),
    'N': (4,8),
    'M': (4,9),
    ',': (4,10),
    ';': (4,10),
    '.': (4,11),
    ':': (4,11),
    '-': (4,12),
    '_': (4,12),
    keyboard.Key.shift_r: (4, 13),
    keyboard.Key.up: (4, 14),
    keyboard.Key.page_up: (4, 14),
    ' ': (5,6), 
}

class CustomEffect:
    def __init__(self, arr, driver):
        self.arr = arr * 0
        self.key_arr = arr * 0
        self.back_light = np.ones((arr.shape[0], arr.shape[1], arr.shape[2])) * 0.1
        self.idx = np.indices((self.arr.shape[0], self.arr.shape[1]), dtype=self.arr.dtype)
        self.driver = driver
        self.phase = np.random.uniform()*np.pi*2
        self.hue = np.random.uniform()*360
        self.mouse = Controller()
        self.old_pos = (0,0)
        self.max_update_fps = 25
        self.iter = 0
        self.color_phase = np.random.random() * np.pi * 2

        # parameters:
        self.cm = plt.get_cmap('hsv')
        self.wave_tickness = 1.2 # 2 for tick and increase value to decrease tickness
        self.wave_width = np.pi*2
        self.phase_increment = 0.0
        self.hue_increment = 1

        self.listener = keyboard.Listener(
            on_press=self.keyboard_on_press,
            on_release=self.keyboard_on_release
        )
        self.listener.start()

    def keyboard_on_press(self, key):
        key_code = key
        if isinstance(key, keyboard.KeyCode):
            key_code = key.char
        try:
            self.key_arr[key_dict[key_code]] = 1.0
        except KeyError:
            print("key not found", key)
    
    def keyboard_on_release(self, key):
        key_code = key
        if isinstance(key, keyboard.KeyCode):
            key_code = key.char
        try:
            self.key_arr[key_dict[key_code]] = 0.0
        except KeyError:
            print("key not found", key)

    def update(self):
        self.iter += 1
        self.arr = self.updatev2()
        if self.iter % (self.get_fps() // self.max_update_fps) == 0:
            return np.clip(0.0, 1.0, self.arr + 0.1 + self.back_light*self.get_crazy_backgroud())
        return

    def updatev1(self):
        self.arr *= 0.9
        x, y = self.get_scaled_mouse_pos()
        y = y * (self.arr.shape[0]-1)
        x = x * (self.arr.shape[1]-1)
        self.arr[round(y), round(x)] = (1.0, 1.0, 1.0)
        self.arr = np.clip(0, 1.0, self.arr + self.key_arr)
        return self.arr

    def updatev2(self):
        decay_rate = 0.04 # https://en.wikipedia.org/wiki/Moving_average#Exponentially_weighted_moving_variance_and_standard_deviation
        self.color_phase += 0.005

        self.arr = (1-decay_rate)*self.arr
        sigma = 1

        x, y = self.get_scaled_mouse_pos()
        y = y * (self.arr.shape[0]-1)
        x = x * (self.arr.shape[1]-1)
        movement_angle = np.arctan2(self.old_pos[1] - y, self.old_pos[0] - x) + np.pi # 0 to 2*pi
        movement_angle = (movement_angle+self.color_phase) % (np.pi*2)
        if self.old_pos[0] == x and self.old_pos[1] == y:
            return np.clip(0.0, 1.0, self.arr + gaussian_filter(self.key_arr, sigma=0.5) * self.get_crazy_backgroud())

        self.old_pos = (x, y)

        color = np.array(self.cm(movement_angle/(2*np.pi))[:-1])
        gaussian_d = np.exp(-0.5 * ((self.idx[0] - y)**2 + (self.idx[1] - x)**2) / sigma**2)
        gaussian_d = gaussian_d / np.max(gaussian_d)
        #,gaussian_d = np.clip(0, 1, gaussian_d*1)
        
        new_arr = np.clip(0.0, 1.0, np.expand_dims(gaussian_d, axis=2) * color.reshape(1, 1, -1))
        self.arr += decay_rate * new_arr
        self.arr = np.max(np.stack([self.arr, new_arr]), axis=0)
        return self.arr

    def get_crazy_backgroud(self):
        arr = self.arr * 0

        # compute sin values and keyboard reference points
        y_ref = np.tile(np.linspace(-1, +1, arr.shape[0]).reshape(-1,1), arr.shape[1])
        x = np.linspace(0, self.wave_width, arr.shape[1])
        y = np.sin(x+self.phase)

        # score higher values for pixels which are closer to keyboard references
        score = np.absolute(y_ref - y)
        score = -score + np.max(score)
        score = self.wave_tickness ** score
        score = score / np.max(score)

        hue = (score*360 + self.hue) % 360
        saturation = np.ones(score.shape)
        value = np.ones(score.shape)

        # hsv to rgb
        hsv = np.moveaxis(np.stack([hue, saturation, value]), 0, 2)
        hsv = np.asarray(hsv, dtype=np.float32)
        arr = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

        self.phase = (self.phase + self.phase_increment) % (np.pi*2)
        self.hue = (self.hue + self.hue_increment) % 360
        return arr

    def get_scaled_mouse_pos(self):
        geometry = self.mouse._display.screen()['root'].get_geometry()._data # some quick dirty hacks. works for xorg
        screen_width = geometry['width']
        screen_height = geometry['height']
        mouse_width = self.mouse.position[0]
        mouse_height = self.mouse.position[1]
        mouse_x = mouse_width / screen_width
        mouse_y = mouse_height / screen_height
        return mouse_x, mouse_y

    def get_fps(self):
        return 60

    def is_enabled(self):
        return True

    def on_exit(self):
        # self.listener.stop() # for some reason this hangs
        pass
